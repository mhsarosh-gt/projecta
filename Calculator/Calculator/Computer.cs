﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Computer
    {
        public int Add(int operand1, int operand2)
        {
            return operand1 + operand2;
        }
        public int Subract(int operand1, int operand2)
        {
            return operand1 - operand2;
        }
        public int Multiply(int operand1, int operand2)
        {
            return operand1 * operand2;
        }
        public float Divide(int operand1, int operand2)
        {
            return operand1 / operand2;
        }
    }
}
