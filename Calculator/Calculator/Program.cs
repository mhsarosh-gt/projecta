﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var title = " Calculator ";
            var total = Console.BufferWidth;
            string asterisks = new string('*', (int)((Console.BufferWidth - title.Length) * 0.5));

            Console.Write(new string('-', total));
            Console.Write("{0}{1}{0}", asterisks, title);
            Console.Write(new string('-', total));

            Computer computer = new Computer();
            int index = 1;
            var methods = computer.GetType().GetMethods()
                .Where(x => x.DeclaringType == computer.GetType())
                .Select(x => new KeyValuePair<int, string>(index++, x.Name));
            foreach (var item in methods)
            {
                Console.WriteLine("Hit '{0}' to {1}", item.Key, item.Value);
            }

            var input = Console.ReadKey().KeyChar;

            if (Char.IsDigit(input))
            {
                int operation = int.Parse(input.ToString());
                KeyValuePair<int, string> operationToPerform = methods.FirstOrDefault(x => x.Key == operation);
                if (operationToPerform.Key == operation)
                {
                    Console.WriteLine("Performing {0}", operationToPerform);
                }
            }

            Console.ReadKey(true);
        }
    }
}
