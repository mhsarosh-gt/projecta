﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculator.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAdd()
        {
            int a = 2, b = 2, sum = 4;
            var computer = new Computer();

            var result = computer.Add(a, b);

            Assert.AreEqual(sum, result);
        }

        [TestMethod]
        public void TestMultiply()
        {
            int a = 2, b = 2, product = 4;
            var computer = new Computer();

            var result = computer.Multiply(a, b);

            Assert.AreEqual(product, result);
        }

        [TestMethod]
        public void TestSubstract()
        {
            int a = 4, b = 2, diff = 2;
            var computer = new Computer();

            var result = computer.Subract(a, b);

            Assert.AreEqual(diff, result);
        }

        [TestMethod]
        public void TestDivide()
        {
            int a = 4, b = 2, quotient = 2;
            var computer = new Computer();

            var result = computer.Divide(a, b);

            Assert.AreEqual(quotient, result);
        }
    }
}
